package cat.itb.cityquiz.Screens.FinalScreen;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.Screens.Start.ScreensViewModel;

public class ScoreScreen extends Fragment {

    private ScreensViewModel mViewModel;

    public static ScoreScreen newInstance() {
        return new ScoreScreen();
    }

    @BindView(R.id.Score)
    TextView scoreText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.score_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(ScreensViewModel.class);
        scoreText.setText(String.valueOf(mViewModel.getScore()));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.PlayAgainButton)
    public void OnViewClicked(){
        Navigation.findNavController(this.getView()).navigate(R.id.play_again);
    }
}
