package cat.itb.cityquiz.Screens.Start;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;
import cat.itb.cityquiz.repository.GameLogic;
import cat.itb.cityquiz.repository.RepositoriesFactory;

public class ScreensViewModel extends AndroidViewModel{
    private GameLogic gameLogic= RepositoriesFactory.getGameLogic();
    private MutableLiveData<Game> game = new MutableLiveData<>();
    private MutableLiveData<Integer> counter = new MutableLiveData<>();
    private AnswerCountDownTimer answerCountDownTimer;

    public ScreensViewModel(@NonNull Application application) {
        super(application);
        answerCountDownTimer = new AnswerCountDownTimer(Game.MAX_MILIS_PER_ANSWER);
        answerCountDownTimer.setTimerChangedListener(this::timeLeft);
    }

    public int getScore(){
        return game.getValue().getNumCorrectAnswers();
    }

    public void startQuiz(){
        game.postValue(gameLogic.createGame(Game.maxQuestions,Game.possibleAnswers));
        answerCountDownTimer.start();
    }

    private void timeLeft(int time) {
        counter.postValue(time);
    }

    public MutableLiveData<Integer> getCounter() {
        return counter;
    }


    public LiveData<Game> getGame(){
        return game;
    }

    public void resposta(int resposta){
        game.postValue(gameLogic.answerQuestions(game.getValue(),resposta));
        answerCountDownTimer.stop();
        answerCountDownTimer.start();
    }

    public void skipQuestion(){
        game.postValue(gameLogic.skipQuestion(game.getValue()));
        answerCountDownTimer.stop();
        answerCountDownTimer.start();
    }
}
