package cat.itb.cityquiz.Screens.Quiz;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.cityquiz.R;
import cat.itb.cityquiz.Screens.Start.ScreensViewModel;
import cat.itb.cityquiz.data.unsplashapi.imagedownloader.ImagesDownloader;
import cat.itb.cityquiz.domain.City;
import cat.itb.cityquiz.domain.Game;
import cat.itb.cityquiz.repository.AnswerCountDownTimer;

public class PlayingScreen extends Fragment {

    @BindView((R.id.marcador))
    TextView marcador;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.FirstOption)
    Button firstOption;
    @BindView(R.id.SecondOption)
    Button secondOption;
    @BindView(R.id.ThirdOption)
    Button thirdOption;
    @BindView(R.id.FourOption)
    Button fourthOption;
    @BindView(R.id.FiveOption)
    Button fiveOption;
    @BindView(R.id.SixOption)
    Button sixOption;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private ScreensViewModel mViewModel;


    public static PlayingScreen newInstance() {
        return new PlayingScreen();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.playing_screen_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(ScreensViewModel.class);
        mViewModel.getGame().observe(this, this::onGameChanged);
        mViewModel.getCounter().observe(this, this::onCounterChanged);

    }

    private void onCounterChanged(Integer integer) {
        progressBar.setProgress(integer);
    }

    private void onGameChanged(Game game) {
        if (game.isFinished()){
            Navigation.findNavController(this.getView()).navigate(R.id.go_final);
        } else {
            List<City> cities = game.getCurrentQuestion().getPossibleCities();
            setButtonCity(cities);
            marcador.setText(((game.getNumCorrectAnswers()) + "/5"));
            setImage(game.getCurrentQuestion().getCorrectCity());
        }

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    private void setButtonCity(List<City> cities) {
        firstOption.setText(cities.get(0).getName());
        secondOption.setText(cities.get(1).getName());
        thirdOption.setText(cities.get(2).getName());
        fourthOption.setText(cities.get(3).getName());
        fiveOption.setText(cities.get(4).getName());
        sixOption.setText(cities.get(5).getName());
    }

    @OnClick(R.id.FirstOption)
    public void OnViewClicked(){
        questionAnserred(0);
    }

    private void questionAnserred(int i) {
        mViewModel.resposta(i);

    }

    @OnClick(R.id.SecondOption)
    public void OnViewClicked2(){
        questionAnserred(1);
    }
    @OnClick(R.id.ThirdOption)
    public void OnViewClicked3(){
        questionAnserred(2);
    }
    @OnClick(R.id.FourOption)
    public void OnViewClicked4(){
        questionAnserred(3);
    }
    @OnClick(R.id.FiveOption)
    public void OnViewClicked5(){
        questionAnserred(4);
    }
    @OnClick(R.id.SixOption)
    public void OnViewClicked6(){
        questionAnserred(5);
    }

    public void setImage(City city){
        String fileName = ImagesDownloader.scapeName(city.getName());
        int resId = getContext().getResources().getIdentifier(fileName, "drawable", getContext().getPackageName());
        imageView.setImageResource(resId);
    }
}
