package cat.itb.cityquiz;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.*;

import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.not;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {

        onView(withId(R.id.StartButton)).perform(click());

        onView(withId(R.id.imageView)).check(matches(isDisplayed()));
        onView(withId(R.id.FirstOption)).perform(click());
        onView(withId(R.id.imageView)).check(matches(isDisplayed()));
        onView(withId(R.id.SecondOption)).perform(click());
        onView(withId(R.id.imageView)).check(matches(isDisplayed()));
        onView(withId(R.id.ThirdOption)).perform(click());
        onView(withId(R.id.imageView)).check(matches(isDisplayed()));
        onView(withId(R.id.FourOption)).perform(click());
        onView(withId(R.id.imageView)).check(matches(isDisplayed()));
        onView(withId(R.id.FiveOption)).perform(click());


        onView(withId(R.id.Score)).check(matches(not(withText(""))));
        onView(withId(R.id.ScoreText)).check(matches(not(withText(""))));
        onView(withId(R.id.PlayAgainButton)).perform(click());

        onView(withId(R.id.StartButton)).perform(click());
        onView(withId(R.id.imageView)).check(matches(isDisplayed()));

    }
}
